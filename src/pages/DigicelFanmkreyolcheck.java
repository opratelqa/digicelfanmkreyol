package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class DigicelFanmkreyolcheck extends TestBaseTG {
	
	final WebDriver driver;
	public DigicelFanmkreyolcheck(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInDigicelFanmkreyol(String apuntaA) {
		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test DigicelFanmkreyol - landing");
		
	
	cargando(500);
	
	
	//WebElement menu1 = driver.findElement(By.xpath("//a[contains(text(), 'Maryaj')]"));
		//menu1.click();

		String URL0 = driver.getCurrentUrl();
		Assert.assertEquals(URL0, "http://www.digicelfanmkreyol.com/" );
		espera(500); 	
		cargando(500);
		
	WebElement menu2 = driver.findElement(By.xpath("//a[contains(text(), 'Bote')]"));
		menu2.click();
		String URL1 = driver.getCurrentUrl();
			Assert.assertEquals(URL1, "http://www.digicelfanmkreyol.com/category/bote/" );
			espera(500); 	
			cargando(500);
			
	WebElement menu3 = driver.findElement(By.xpath("//a[contains(text(), 'Mòd')]"));
		menu3.click();
		String URL2 = driver.getCurrentUrl();
			Assert.assertEquals(URL2, "http://www.digicelfanmkreyol.com/category/mod/" );
			espera(500);
			cargando(500);
			
	WebElement menu4 = driver.findElement(By.xpath("//a[contains(text(), 'Manman')]"));
		menu4.click();
		String URL3 = driver.getCurrentUrl();
				Assert.assertEquals(URL3, "http://www.digicelfanmkreyol.com/category/manman/" );
				espera(500);
				cargando(500);
				
	WebElement menu5 = driver.findElement(By.xpath("//a[contains(text(), 'Alimantasyon')]"));
		menu5.click();
		String URL4 = driver.getCurrentUrl();
				Assert.assertEquals(URL4, "http://www.digicelfanmkreyol.com/category/alimantasyon/" );
				espera(500);
				cargando(500);
				
	WebElement menu6 = driver.findElement(By.xpath("//a[contains(text(), 'Kay Ak Fanmi')]"));
		menu6.click();
		String URL5 = driver.getCurrentUrl();
				Assert.assertEquals(URL5, "http://www.digicelfanmkreyol.com/category/kay-ak-fanmi/" );
				espera(500);
				cargando(500);
				
	WebElement menu7 = driver.findElement(By.xpath("//a[contains(text(), 'Sante')]"));
		menu7.click();
		String URL6 = driver.getCurrentUrl();
				Assert.assertEquals(URL6, "http://www.digicelfanmkreyol.com/category/sante/" );
				espera(500);
				cargando(500);
				
	WebElement menu8 = driver.findElement(By.xpath("//a[contains(text(), 'Sèks')]"));
		menu8.click();
		String URL7 = driver.getCurrentUrl();
				Assert.assertEquals(URL7, "http://www.digicelfanmkreyol.com/category/seks/" );
				espera(500);
				cargando(500);
				
	driver.get("http://www.digicelfanmkreyol.com/ed-ak-kesyon/");
	
	System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Digicel Fanm Kreyol"); 
	System.out.println();
	System.out.println("Fin de Test DigicelFanmkreyol - Landing");
		

	}		

}  

